var fs = require('fs');
var path = require('path');
var util = require('util');
var assert = require('assert');
var debug = require('debug')('app:logic');

/**
 * @typedef {Object} Venue
 *
 * @property {String} name
 * @property {String} eventful_id
 * @property {String} address
 * @property {String} city
 * @property {String} zip
 * @property {String} state
 * @property {String} country
 * @property {String} country_abr
 * @property {Number[]} location
 *
 */

/**
* @typedef {Object} Show
*
* @property {Date} time
* @property {Venue} venue
* @property {Artist} artist
*
*/

/**
*   @typedef {Object} Artist
*
*   @property {String}  spotify_id
*   @property {String}  eventful_id
*   @property {String}  name
*   @property {String}  top_track
*   @property {String}  image_url
*/

/**
 *
 * @param   {Object} artist_json  JSON represenation of an artist
 * @returns {Artist}              an artist object
 *
 */
function getArtistObject(artist_json) {
  try {
    return {
      eventful_id: artist_json.id,
      name: artist_json.name
    };
  } catch (error) {
    throw __filename + "::getArtistObject: Invalid JSON format";
  }
}

/**
 *
 * @param {Object} event_json a json object from the eventful api
 * @returns {Venue} a venue object
 */
function getVenueObject(event_json){
  return {
    name: event_json.venue_name,
    eventful_id: event_json.id,
    address: event_json.venue_address,
    city: event_json.city_name,
    zip: event_json.postal_code,
    state: event_json.region_name,
    country: event_json.country_name,
    country_abr: event_json.country_abbr,
    location: [event_json.latitude, event_json.longitude]
  };
}

/**
 * @param   {Object} json_data
 *          Eventful response object (see: https://api.eventful.com/docs/events/search)
 * @returns {Show[]}
 *          Array of shows
 */
module.exports = function(json_data) {
  var shows = [];

  assert(json_data.events);

  for(var i=0; i<json_data.events.event.length; i++){
    assert(json_data.events.event[i]);
    
    var event = json_data.events.event[i];
    var datetime = Date.parse(event.start_time);
    var performers = event.performers;
    
    if(performers == null) continue; 

    if(util.isArray(performers.performer)){
      for(var j=0; j<performers.performer.length; j++) {
        shows.push({
          artist: getArtistObject(performers.performer[j]),
          venue: getVenueObject(event),
          date: datetime
        });
      }
    } else {
      shows.push({
        artist: getArtistObject(performers.performer),
        venue: getVenueObject(event),
        date: datetime });
      }
    }
  

  return shows;
};
