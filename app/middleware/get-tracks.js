var path = require('path');
var SpotifyWebApi = require('spotify-web-api-node');
var request = require('request-promise');
var debug = require('debug')('app:logic');
var assert = require('assert');
var datetime = require('date-and-time');
var geoip = require("geoip-lite");

var parseShows = require(path.join(__dirname, 'parse-response'));


/**
 * Searches for events using Eventful's /search endpoint
 *
 * @param   {Number[]} geo        An array representing geo coordinates [lat, long]
 * @param   {Date}     start_date Date object for start of time window
 * @param   {Date}     end_date Date object for end of time window
 * @param   {Number}
 *
 * @returns {Promise} promise object that represents an array of shows
 */
function getShows(geo, start_date, end_date, radius_km=40, _page_size=5, _page_number=1) {
  assert(start_date < end_date, `Invalid date range ${start_date}-${end_date}`);

  var formattedToday = datetime.format(start_date, 'YYYYMMDD');
  var formattedMonthFromToday = datetime.format(end_date,'YYYYMMDD');
  var EVENTFUL_URI = "https://api.eventful.com/json/events/search";

  var query = {
    q: "music",
    date: formattedToday + "00" + "-" +     // "00" padding: quirk of the Eventful API
          formattedMonthFromToday + "00",
    l: geo[0] + "," + geo[1],
    within: `${radius_km}`,
    units: "km",
    app_key: process.env.EVENTFUL_API_KEY,
    page_size: _page_size,
    sort_order: "popularity",
    page_number: _page_number,
  };

  debug(`${EVENTFUL_URI}?` +
        `q=${query.q}&` +
        `data=${query.date}&` +
        `l=${query.l}&` +
        `within=${query.units}&` +
        `app_key=${query.app_key}&` +
        `units=${query.units}&` +
        `page_size=${query.page_size}&` +
        `sort_order=${query.sort_order}`);

  return request({
    uri: EVENTFUL_URI,
    qs: query,
  }).then((response) => {
    var json_data = JSON.parse(response);
    assert(json_data.events, "getShows: No shows retrieved");

    return parseShows(json_data);
  });
}

/**
 * Retrieves artist data for an artist query from Spotify's /search endpoint
 *
 * @param {Show} show A show object
 * @returns {Show} Show with artist_id attribute
 */
function getArtistInfo(show) {
  return spotifyApi.searchArtists(show.artist.name, {limit: 1})
    .then(function (data) {
      // Some shows don't have artists or don't have artists with Spotify pages
      // We'll delete those shows with a filter
      if (data.body.artists.items.length == 0) {
        return null;
      }

      show.artist.spotify_id = data.body.artists.items[0].id;
      var images = data.body.artists.items[0].images;

      if(images.length > 0) {
        show.artist.image_url = images[0].url;
      }

      return show;
    }, function (err) {
        console.log('getArtistInfo: Something went wrong!', err);
    });
}

/**
 * Retrieves track data for an artist from Spotify's /artist endpoint
 *
 * @param {Show} show A show object
 * @returns {Show} Show with track_id attribute
 *
 */
function getTopTrack(show){
  return spotifyApi.getArtistTopTracks(show.artist.spotify_id, 'from_token')
    .then(function (data) {
      assert(data.body.tracks[0], "getTopTrack: No tracks retrieved");
      show.artist.top_track = {
        spotify_id: data.body.tracks[0].id,
        name: data.body.tracks[0].name
      };
      return show;
    }, function (err) {
        console.log('getTopTrack: Something went wrong!', err);
    });
}

module.exports = function (req, res, next) {
  spotifyApi = new SpotifyWebApi();
  spotifyApi.setAccessToken(req.query.access_token);

  // TODO: Get IP from request
  // var ip_addr = req.ip;
  var ip_addr = "73.164.10.145";
  var loc = geoip.lookup(ip_addr).ll;

  var start_date = new Date();

  var MONTH_MS = 2592000000; // 1000*60*60*24*30;
  var MONTH_FROM_TODAY = Date.now() + MONTH_MS;
  var end_date = new Date(MONTH_FROM_TODAY);
  var radius_km = 40;

  res.shows=null;

  getShows(loc, start_date, end_date, radius_km, 15, 2)
    .map(getArtistInfo)
    .then((shows) => {
      return shows.filter((show) => {
        return show != null })
    })
    .map(getTopTrack)
    .then((shows) => {
      res.shows = shows;
      res.location = loc;
      next();
    }, (err) => {
      console.log("Couldn't retreive show data! " + err.stack);
    });
};
