var express = require('express');
var router = express.Router();
var debug = require('debug')('app:carousel');

/* GET home page. */
router.get('/', function (req, res, next) {
  debug(res.shows);
  debug(res.shows.length);
  res.render('carousel-layout', {
    access_token: req.query.access_token,
    shows: res.shows,
    location: res.location
  });
});

module.exports = router;
