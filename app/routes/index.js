var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Implicit Grant flow',
                        prompt: 'Log in with Spotify'
                      });
});

module.exports = router;
