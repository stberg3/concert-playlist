var express = require('express');
var router = express.Router();
var debug = require('debug')('app:view');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('widget-index-player', {
    access_token: req.query.access_token,
    shows: res.shows
  });
});

module.exports = router;
