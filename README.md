# Concert Playlist
The idea here is to gather data about upcoming concerts, viz. who's coming to
and when, and making a playlist containing those artists.

There are three tasks here: getting location data, getting concert information,
and creating the playlist.


## Getting Location Data
This problem keeps coming up... I would like to get location from IP because the
user doesn't have to do anything, but going by zip code is another option. I'm
not very interested in this task since the driver behind this application is my
desire to get a playlist for *me* that contains songs from artists coming to
a venue near me. As such, I'm going to ignore this problem until I can get the
core functionality working.

## Getting Concert Information
*Eventful API*
- While potentially limited in scope, this API is friendly, if not
  well-documented.

- [LINK](api.eventful.com)

## Creating the Playlists
This looks like the easy part. Spotify has a baked-in playlist endpoint, so I
can use that.

The only issue I foresee is typographical differences between the
artists' names on events and their names on Spotify. If that's an issue I'll
just skip them because I'm lazy. If the application ends up coming together I
can spend time adding logic to address this issue.

Link to Spotify's API documentation:

- [LINK](https://developer.spotify.com/documentation/web-api/quick-start/)
